import { NgModule } from '@angular/core';
import { RsvAtomicComponent } from './rsv-atomic.component';
import { LogoComponent } from './logo/logo.component';
import { PathCardComponent } from './path-card/path-card.component';

@NgModule({
  declarations: [
    RsvAtomicComponent,
    LogoComponent,
    PathCardComponent
  ],
  imports: [
  ],
  exports: [
    RsvAtomicComponent,
    LogoComponent
  ]
})
export class RsvAtomicModule { }
