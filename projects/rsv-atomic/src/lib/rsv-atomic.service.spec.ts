import { TestBed } from '@angular/core/testing';

import { RsvAtomicService } from './rsv-atomic.service';

describe('RsvAtomicService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RsvAtomicService = TestBed.get(RsvAtomicService);
    expect(service).toBeTruthy();
  });
});
