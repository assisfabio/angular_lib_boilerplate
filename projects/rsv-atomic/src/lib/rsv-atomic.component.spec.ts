import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RsvAtomicComponent } from './rsv-atomic.component';

describe('RsvAtomicComponent', () => {
  let component: RsvAtomicComponent;
  let fixture: ComponentFixture<RsvAtomicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RsvAtomicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RsvAtomicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
