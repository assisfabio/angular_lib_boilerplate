import { storiesOf } from '@storybook/angular';
import { boolean, number, text, withKnobs } from '@storybook/addon-knobs';

import { PathCardComponent } from './path-card.component';

import { pathCardMD } from './path.notes';

storiesOf('Path Card', module)
.addDecorator(withKnobs)
.add('Air Card', () => ({
  component: PathCardComponent,
  props: {
    bgcolor: text('text', '#f1f1f1'),
  },
}), {
  notes: {
    markdown: pathCardMD
  }
});
