import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'rsv-path-card',
  templateUrl: './path-card.component.html',
  styleUrls: ['./path-card.component.scss']
})
export class PathCardComponent implements OnInit, OnChanges {

  @Input()
  bgcolor: string;
  
  active: boolean = false;
  defaultBgColor: string = "#FFFFFF";
  style: any;
  
  constructor() { }
  
  ngOnInit() {
    this.style = {
      'background-color': this.defaultBgColor
    };
  }

  ngOnChanges(changes: SimpleChanges){
    console.log(changes);
  }

  toggleActive() {
    this.active = !this.active;

    this.style['background-color'] = (this.active) ? this.bgcolor : this.defaultBgColor;
  }
}
