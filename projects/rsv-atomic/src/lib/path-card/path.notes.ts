export const pathCardMD = `
inicio


* * *

***

*****

- - -

---------------------------------------

end


**Títulos**

# H1

## H2

### H3

#### H4

Texto Simples

*Texto enfatizado*

**Texto fortemente enfatizado**

\`Codigo inline\`

\`codigo\`


**Lista não ordenada**

 - Item
  - subitem
 - Item

**Lista não ordenada**

1. Item
 1. subitem
1. Item


**Bloco de Texto**

> 
> This is a blockquote with two paragraphs. Lorem ipsum dolor sit amet,
> consectetuer adipiscing elit. Aliquam hendrerit mi posuere lectus.
> Vestibulum enim wisi, viverra nec, fringilla in, laoreet vitae, risus.
> 
> ### With title
> > This is a blockquote inside another blockquote. Donec sit amet nisl. Aliquam semper ipsum sit amet velit. Suspendisse
> > id sem consectetuer libero luctus adipiscing.



**Bloco de Código**

    function () {
        // Do Something
        return false;
    }

**Outro Bloco de Código**

    const $foo: any => {
        // Do Something
    }

**Links**

I get 10 times more traffic from [Google](http://google.com/ "Google")
than from [Yahoo](http://search.yahoo.com/ "Yahoo Search") or
[MSN](http://search.msn.com/ "MSN Search").
`;