import { storiesOf } from '@storybook/angular';
import { boolean, number, text, withKnobs } from '@storybook/addon-knobs';

import { LogoComponent } from './logo.component';

import { logoMD } from './logo.notes';

storiesOf('Logo Fractal', module)
.addDecorator(withKnobs)
.add('Simple Logo', () => ({
  component: LogoComponent,
  props: {
    text: text('text', 'Hello Storybook')
  },
}), {
  notes: {
    markdown: logoMD
  }
});
