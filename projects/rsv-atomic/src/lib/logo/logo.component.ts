import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'rsv-logo',
  templateUrl: './logo.component.html',
  styleUrls: ['./logo.component.scss']
})
export class LogoComponent implements OnInit {

  @Input() text: String;
  constructor() { }

  ngOnInit() {
  }

}
