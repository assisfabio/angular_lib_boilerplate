import { addParameters, configure } from '@storybook/angular';
import rsvTheme from './theme';

addParameters({
  options: {
    theme: rsvTheme,
  },
});

// automatically import all files ending in *.stories.ts
const req = require.context('../projects', true, /\.stories\.ts$/);
function loadStories() {
  req.keys().forEach(filename => req(filename));
}

configure(loadStories, module);
