import { create } from '@storybook/theming';

export default create({
  base: 'light',
  brandTitle: 'F2P Labs',
  brandUrl: 'https://f2p.com.br',
  brandImage: 'https://placehold.it/350x150',
});